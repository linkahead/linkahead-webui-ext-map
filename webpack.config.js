const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack')

function get_env() {
  // for development
  const env = {}
  env["GRPC_API_URI"] = "http://localhost:8081/api"
  return Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
  }, {});
}

module.exports = function(env, argv) {
const config = {
  entry: path.resolve(__dirname, './src/index.js'),
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-url-loader",
            options: {
              limit: 1000,
            },
          },
        ],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      "react": path.resolve(__dirname, './node_modules/react')
    }
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'caosdb-webui-ext-map.js',
    library: 'CaosDBExtMap',
    libraryTarget: 'umd',
  },
  devServer: {
    port: 8082,
    static: [
      path.resolve(__dirname, "./dist"),
      path.resolve(__dirname, "./static"),
    ],
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },
  externals: {
    react: {
      commonjs: "react",
      commonjs2: "react",
      amd: "React",
      root: "React"
    },
    reactDom: {
      commonjs: "react-dom",
      commonjs2: "react-dom",
      amd: "ReactDom",
      root: "ReactDom"
    },
    "@indiscale/caosdb-webui-core-components": {
      commonjs: "@indiscale/caosdb-webui-core-components",
      commonjs2: "@indiscale/caosdb-webui-core-components",
      amd: "CaosDBCoreComponents",
      root: "CaosDBCoreComponents"
    },
  },
  plugins: [
    new HTMLWebpackPlugin({
      favicon: false,
      showErrors: true,
      cache: true,
      template: path.resolve(__dirname, 'public/index.html')
    }),
  ]
};

if(env["include-peer-deps"]) {
  // for development
  config.externals = {}
  console.log(get_env())
  config.plugins.push(new webpack.DefinePlugin(get_env()))
  config.plugins.push(new webpack.ProvidePlugin({
             process: 'process/browser',
      }))
  config.entry = path.resolve(__dirname, './public/mock.js')
}

return config;
}
