import PropTypes from "prop-types";
import { logger } from "../logging";
import L from "leaflet";
import { useMap } from "react-leaflet";

L.Control.ViewChangeControl = L.Control.extend({
  _button: undefined,
  _make_view_menu: function () {
    var form = L.DomUtil.create("form", "viewMenu d-none");

    form.addEventListener("click", function (e) {
      // just stop the click here, such that it does not change
      // anything else in the map's state.
      e.stopPropagation();
    });

    return form;
  },
  onAdd: function () {
    const view_menu = this._view_menu;
    var click = (e) => {
      e.stopPropagation();
      if (L.DomUtil.hasClass(view_menu, "d-none")) {
        L.DomUtil.removeClass(view_menu, "d-none");
      } else {
        L.DomUtil.addClass(view_menu, "d-none");
      }
    };

    var button = L.DomUtil.create(
      "div",
      "leaflet-bar leaflet-control leaflet-control-custom caosdb-f-map-change-view-btn"
    );
    button.title = "Change the view";
    button.addEventListener("click", click);

    button.appendChild(view_menu);

    const icon = L.DomUtil.create("i", "bi-three-dots-vertical");
    button.appendChild(icon);

    this._button = button;
    return this._button;
  },

  setViews: function (views, current_view, set_map_view_cb) {
    logger.trace("setViews", views, current_view, set_map_view_cb);
    this._view_menu = this._make_view_menu();
    views.forEach((view) => {
      const option = L.DomUtil.create("div", "caosdb-f-map-view-select");
      option.title = view.description || view.name || view.id;

      const input = L.DomUtil.create("input");
      input.name = "view";
      input.type = "radio";
      input.value = view.id;
      if (view.id === current_view) {
        input.checked = true;
      }

      const label = L.DomUtil.create("label");
      label.innerHTML = view.name || view.description || view.id;

      option.appendChild(input);
      option.appendChild(label);
      this._view_menu.appendChild(option);
    });

    this._view_menu.addEventListener("change", (e) => {
      logger.trace("ViewChangeControl.onChange", e);
      set_map_view_cb(e.target.value);
    });
  },
});

L.control.viewChangeControl = function (options) {
  return new L.Control.ViewChangeControl(options);
};

export function ViewChangeControl({ views, mapView, setMapView }) {
  const map = useMap();

  const viewChangeControl = L.control.viewChangeControl({
    position: "bottomleft",
  });

  if (!map._viewChangeControl) {
    map._viewChangeControl = viewChangeControl;
    viewChangeControl.setViews(views, mapView, setMapView);
    viewChangeControl.addTo(map);
  }

  return null;
}

ViewChangeControl.propTypes = {
  views: PropTypes.array,
  mapView: PropTypes.string,
  setMapView: PropTypes.func,
};
