import PropTypes from "prop-types";
import L from "leaflet";
import "../contrib/latlnggraticule/leaflet.latlng-graticule.js";
import "../contrib/simplegraticule/L.Graticule.js";
import { useMap } from "react-leaflet";
import { logger } from "../logging";

export function Graticule({ config }) {
  const map = useMap();

  if (config.type === "latlngGraticule") {
    L.latlngGraticule(config.options).addTo(map);
  } else if (typeof config.type === "undefined" || config.type === "simple") {
    L.graticule(config.options).addTo(map);
  } else {
    logger.warn("unknown graticule type: ", config.type);
  }

  return null;
}

Graticule.propTypes = {
  config: PropTypes.shape({
    options: PropTypes.object,
    type: PropTypes.oneOf(["simple", "latlngGraticule"]),
  }),
};
