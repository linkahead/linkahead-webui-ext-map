import PropTypes from "prop-types";
import { logger } from "../logging";
import L from "leaflet";
import { useMap } from "react-leaflet";

/**
 * Return a new leaflet control for setting paths to use for geo location
 *
 * @param {function} callback - a callback applies the effect of a
 *                              changed path
 * @param {string[]} paths - ids of the paths defined for this map.
 *
 * @returns {L.Control} the drop down menu button.
 */
const get_path_ddm = function (callback, paths) {
  logger.trace("get_path_ddm", callback, paths);

  var path_ddm = L.Control.extend({
    options: {
      position: "bottomright",
    },

    onAdd: function () {
      return this.button;
    },

    setValue: function (value) {
      this.button.value = value;
    },

    button: (function () {
      var select = L.DomUtil.create(
        "select",
        "leaflet-bar leaflet-control leaflet-control-custom caosdb-f-map-path-drop-down"
      );
      select.title = `Show the location of related entities.
By default ('Same Entity') entities are shown that have
a geographic location. The other options allow to show
entities on the map using the location of a related
entity.`;

      let tmp_html = '<option value="same">Same Entity</option>';
      for (let pa in paths) {
        tmp_html += `<option value="${pa}">${pa}</option>`;
      }
      select.innerHTML = tmp_html;

      L.DomEvent.on(select, "change", (e) => {
        callback(e);
      });
      return select;
    })(),
  });
  return new path_ddm();
};

const path_drop_down_handler = {
  _control: undefined,
  _callback: console.log,
  _paths: [],
  _current_path: undefined,

  addHooks: function () {
    this._control = get_path_ddm(this._callback, this._paths);
    this._map.addControl(this._control);
  },

  removeHooks: function () {
    this._map.removeControl(this._control);
  },

  setCurrentPath: function (path) {
    logger.trace("setCurrentPath", this, path);
    this._control.setValue(path);
  },
};

export function PathDropDown({ setCurrentPath, currentPath, paths }) {
  logger.trace("PathDropDown", setCurrentPath, currentPath, paths);
  const map = useMap();

  if (!map.path_drop_down) {
    path_drop_down_handler._callback = (e) => {
      setCurrentPath(e.target.value);
    };
    path_drop_down_handler._paths = paths;
    const handler = L.Handler.extend(path_drop_down_handler);

    map.addHandler("path_drop_down", handler);
    map.path_drop_down.enable();
  }

  map.path_drop_down.setCurrentPath(currentPath);

  return null;
}

PathDropDown.propTypes = {
  setCurrentPath: PropTypes.func,
  currentPath: PropTypes.string,
  paths: PropTypes.object,
};
