import PropTypes from "prop-types";
import { logger } from "../logging";
import L from "leaflet";
import { useMap } from "react-leaflet";
import { get_with_POV } from "../Map.helpers";
import SelectSearchIcon from "../select-search.min.svg";

/**
 * Plug-in for leaflet which lets the user select an area in the map
 * and execute a query using a latitude/longitude filter for entities.
 *
 * This handler adds a select button as a control to the map. The
 * select button toggles and indicates the select mode (on/off).
 *
 * The selection can be started by either clicking on the map when the
 * select mode is on (after enabling it via the select button).
 *
 * The query button apears as soon as an selection has being finished.
 *
 * The query button generates a query from for searching inside the
 * selected area and calls the `queryCallback` function.
 */
const select_handler = {
  /**
   * Initialize the handler after it has been added to the map.
   *
   * This function is called by the map after the handler has been
   * added via {@link L.Map.addHandler} or the handler added itself
   * to the map via {@link L.Handler.addTo}.
   *
   * This method
   * 1) Adds the select button.
   * 2) Adds a listener for the `mousedown` event.
   *
   * Both are means to start the process of selecting an area in the
   * map.
   */
  addHooks: function () {
    const select_button = this._get_select_button((event) => {
      logger.trace("select button clicked", this);
      event.preventDefault();
      event.stopPropagation();
      this._toggle_select_mode();
    });
    this._select_button = select_button;
    this._map.addControl(select_button);
    this._map.on("mousedown", this._mousedown_listener);
  },

  /**
   * Clean up after the select handler has been removed from the map.
   *
   * This method removes the select button and the `mousedown`
   * listener.
   */
  removeHooks: function () {
    this._select_button.remove();
    this._map.off("mousedown", this._mousedown_listener);
  },

  /**
   * Change the color of the select button in order to highlight it.
   *
   * This is used to indicate that the select mode is on and that the
   * user can select something by clicking and moving the mouse on
   * the map.
   */
  _highlight_select_button: function () {
    this._select_button.button.classList.add("highlight");
  },

  /**
   * Change the color of the select button back to normal.
   */
  _unhighlight_select_button: function () {
    this._select_button.button.classList.remove("highlight");
  },

  /**
   * Toggle the select mode (on/off).
   *
   * This includes setting the _select_mode_on to true/false,
   * highlighting/unhighlighting the select button and
   * disabling/enabling the moving of the map center by dragging.
   */
  _toggle_select_mode: function () {
    logger.trace("toggle select mode", this);
    if (this._select_mode_on) {
      this._unhighlight_select_button();
      this._map.dragging.enable();
      this._reset_selection();
      this._select_mode_on = false;
    } else {
      this._highlight_select_button();
      this._map.dragging.disable();
      this._select_mode_on = true;
    }
  },

  /**
   * Return a button for toggling and indicating the select mode.
   *
   * The select button shows a litte dashed square as its icon.
   *
   * @param {function} callback - a callback which toggles the select
   *     mode.
   * @returns {L.Control} the select button.
   */
  _get_select_button: function (callback) {
    // TODO flatten the structure of the code and possibly merge it with the query_button code.
    var select_button = L.Control.extend({
      options: {
        position: "topleft",
      },

      onAdd: function () {
        return this.button;
      },

      button: (function () {
        var button = L.DomUtil.create(
          "div",
          "leaflet-bar leaflet-control leaflet-control-custom caosdb-f-map-select-search-btn"
        );
        button.title = "Select and area and search.";
        button.addEventListener("click", callback);

        const icon = L.DomUtil.create("img", "");
        icon.src = SelectSearchIcon;
        button.appendChild(icon);

        button.onmousedown = (event) => {
          event.stopPropagation();
        };
        button.onmouseup = (event) => {
          event.stopPropagation();
        };
        return button;
      })(),
    });
    return new select_button();
  },

  /**
   * Return a button for opening the query panel with a pre-filled query.
   *
   * The query button has a loupe icon.
   *
   * The query button is added after an area has been selected and
   * only visible as long an area is selected.
   *
   * @param {function} callback - a callback for opening the query
   *     panel and fill in the query.
   * @returns {L.Control} the query button.
   */
  _get_query_button: function (callback) {
    var query_button = L.Control.extend({
      options: {
        position: "topleft",
      },

      onAdd: function () {
        return this.button;
      },

      button: (function () {
        var button = L.DomUtil.create(
          "div",
          "leaflet-bar leaflet-control leaflet-control-custom caosdb-f-map-search-btn"
        );
        button.title = "Search within this area";
        button.innerHTML = '<i class="bi-search"></i>';
        button.onclick = callback;

        button.onmousedown = (event) => {
          event.stopPropagation();
        };
        button.onmouseup = (event) => {
          event.stopPropagation();
        };
        return button;
      })(),
    });
    return new query_button();
  },

  /**
   * Listens on the mousedown event of the map and calls the
   * _startSelect method if either (1) the select mode is on or (2)
   * the shift key is pressed during the click on the map.
   */
  _mousedown_listener: function (event) {
    logger.trace("mousedown", event, "on", this);
    if (!event.originalEvent.shiftKey && !this.select._select_mode_on) {
      return;
    }
    event.originalEvent.preventDefault();
    event.originalEvent.stopPropagation();
    this.select._startSelect(event.latlng);
  },

  /**
   * Remove a pre-existing selection and start the process of a new
   * selection.
   *
   * When the user clicks on the map with shift key pressed or
   * _select_mode this method is called with the coordinates of the
   * click.
   *
   * This also adds listeners on `mousemove` events (for redrawing
   * the selected area) and on `mouseup` events for finishing the
   * process of selection.
   *
   * @param {L.LatLng} start_point - the coordinates where the
   *     selection begins.
   */
  _startSelect: function (start_point) {
    this._reset_selection();
    this._point1 = start_point;
    logger.trace("point1", this._point1);

    this._map.on("mousemove", this._drawRect);
    this._map.on("mouseup", this._endSelect);
  },

  /**
   * If present, remove the selected area and the query button from
   * the map.
   */
  _reset_selection: function () {
    this._point1 = undefined;
    if (this._rectangle) {
      this._rectangle.remove();
    }
    this._remove_query_button();
  },

  /**
   * (Re-)draw the rectangle which indicates the currently selected
   * area.
   *
   * This method is added as a listener on the `mousemove` event by
   * the _startSelect method.
   */
  _drawRect: function (event) {
    logger.trace("mousemove", event, "on", this);
    event.originalEvent.preventDefault();
    event.originalEvent.stopPropagation();

    // remove old rectangle
    if (this.select._rectangle) {
      this.select._rectangle.remove();
    }

    // draw new rectangle
    const point2 = event.latlng;
    const area = this.select._get_area(this.select._point1, point2);
    this.select._rectangle = this.select._get_select_rectangle(area);
    this.select._rectangle.addTo(this);
  },

  /**
   * Return a colored rectangle covering an area.
   *
   * @param {L.LatLngBounds} area.
   * @returns {L.Rectangle} the colored rectangle.
   */
  _get_select_rectangle: function (area) {
    return L.rectangle(area, {
      color: "#ff7800",
      weight: 1,
    });
  },

  /**
   * Finish the process of selection, add the query button to the map.
   *
   * This method is a listener on the `mouseup` event and is added by
   * the _startSelect method.
   *
   * It removes itself as a listener and also the _drawRect listener
   * on the `mousemove` event.
   */
  _endSelect: function (event) {
    logger.trace("mouseup", event, "on", this);
    event.originalEvent.preventDefault();
    event.originalEvent.stopPropagation();
    this.off("mouseup", this.select._endSelect);
    this.off("mousemove", this.select._drawRect);

    const point2 = event.latlng;
    const point1 = this.select._point1;
    logger.trace("point1", point1);
    logger.trace("point2", point2);

    this.select._point1 = undefined;
    if (point2.lat === point1.lat && point2.lng === point1.lng) {
      return;
    }
    const area = this.select._get_area(point1, point2);
    this.select._add_query_button(area);
  },

  /**
   * Add a `query` button to the map (showing a loupe icon) which
   * opens the query panel with a pre-filled query.
   *
   * The generated query searches for entities inside the selected
   * area `a`.
   *
   * A pre-existing query button is removed and the new query button
   * is stored into this._query_button for later references.
   *
   * @param {L.LatLngBounds} a - the selected area.
   * @return {L.Control} the new query button.
   */
  _add_query_button: function (a) {
    logger.trace("_add_query_button", a, this);

    // remove older query button
    this._remove_query_button();

    const north = this._round(a.getNorth());
    const south = this._round(a.getSouth());
    const east = this._round(a.getEast());
    const west = this._round(a.getWest());
    const query = this.generate_query_from_bounds(north, south, west, east);

    // generate a call-back which opens the query panel with the
    // generated query
    const callback = (event) => {
      logger.trace("click query_button", query, this);
      event.stopPropagation();
      this._query_callback(query);
    };

    this._query_button = this._get_query_button(callback);
    this._map.addControl(this._query_button);
    return this._query_button;
  },

  /**
   * Remove a `query` button if present.
   *
   * @return {L.Control} the old query button if present, `undefined`
   *     otherwise.
   */
  _remove_query_button: function () {
    const old = this._query_button;
    if (old) {
      old.remove();
      this._query_button = undefined;
      return old;
    }
  },

  /**
   * Return the area specified by two coordinates.
   *
   * The edges are parallel to the latitude and longitude of the two
   * points.
   *
   * @param {L.LatLng} point1
   * @param {L.LatLng} point2
   * @returns {L.LatLngBounds} the area.
   */
  _get_area: function (point1, point2) {
    return L.latLngBounds(point1, point2);
  },

  /**
   * Round the double value to 3 decimal places.
   *
   * Note: This function is used to round map coordinates to
   * meaningful values.
   *
   * @param {number} d - a double value
   * @returns {number} a rounded double value.
   */
  _round: function (d) {
    return Math.round(d * 1000) / 1000;
  },

  /**
   * The first point of the selected area.
   *
   * It is stored by the _startSelect method is used by subsequent
   * execution of the _drawRect method and eventually the _endSelect
   * method.
   */
  _point1: undefined,

  /**
   * _select_mode_on indicates whether clicks on the map without
   * pressing shift will start/end/reset selection.
   */
  _select_mode_on: false,

  _datamodel: undefined,

  /**
   * Generate a query to search inside a map area bounded by maximum and
   * minimum latitudes and longitudes.
   *
   * This function uses the configuration from {@link SelectConfig} for
   * the role and entity, the configuration from {@link DataModelConfig}
   * for the latitude and longitude properties, and constructs a query
   * filter from the rectangular bounding box such that all entities with
   * coordinates inside that area are returned.
   *
   * The area is specified as follows:
   *
   * <code>
   *                  north
   *            +-----------------+
   *            |                 |
   *            |                 |
   *        west|                 |east
   *            |                 |
   *            |                 |
   *            +-----------------+
   *                  south
   * </code>
   *
   * The horizontal lines (-) mark the maximum and minimum latitude and
   * the vertical lines (|) mark the maximum and minimum longitude of the
   * area to be searched in.
   *
   * @param {number} north
   * @param {number} south
   * @param {number} west
   * @param {number} east
   * @return {string} a query string.
   */
  generate_query_from_bounds: function (north, south, west, east) {
    const role = this._datamodel.role;
    var entity = this._datamodel.entity;
    const lat = this._datamodel.lat;
    const lng = this._datamodel.lng;

    let path = this._current_path;
    if (path && path.length > 0 && entity == "") {
      entity = path[0];
    }

    var additional_path = "";
    if (path && path.length > 1) {
      additional_path = get_with_POV(path.slice(1, path.length));
    }

    const query_filter =
      " ( " +
      lat +
      " < '" +
      north +
      "' AND " +
      lat +
      " > '" +
      south +
      "' AND " +
      lng +
      " > '" +
      west +
      "' AND " +
      lng +
      " < '" +
      east +
      "' ) ";

    const query =
      "FIND " + role + " " + entity + additional_path + " WITH " + query_filter;
    return query;
  },

  _query_callback: (query) => {
    logger.error("Unconfigured queryCallback", query);
  },
};

export function SearchControl({ currentPath, datamodel, queryCallback }) {
  const map = useMap();

  if (!map.select) {
    map.addHandler("select", L.Handler.extend(select_handler));
    map.select.enable();
  }

  map.select._datamodel = datamodel;
  map.select._query_callback = queryCallback;
  map.select._current_path = currentPath;

  return null;
}

SearchControl.propTypes = {
  currentPath: PropTypes.arrayOf(PropTypes.string),
  datamodel: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
    role: PropTypes.string,
    entity: PropTypes.string,
  }),
  queryCallback: PropTypes.func,
};
