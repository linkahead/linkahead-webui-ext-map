import L from "leaflet";
import { useMap } from "react-leaflet";
import "leaflet.coordinates/dist/Leaflet.Coordinates-0.1.5.min.js";
import { logger } from "../logging";

export function Coordinates() {
  const map = useMap();

  const coordinates = L.control.coordinates({
    // TODO move to config
    position: "bottomleft",
    decimals: 2,
    enableUserInput: false,
    useDMS: true,
  });

  if (!map._coordinates) {
    logger.trace("Coordinates", map);
    map._coordinates = coordinates;
    coordinates.addTo(map);
  }

  return null;
}
