import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import { logger } from "../logging";
import { Await } from "@indiscale/caosdb-webui-core-components";
import { get_map_config, get_local_config } from "../MapConfig";

/**
 * ON: map is visible, toggle button click will be processed
 * OFF: map is not visible, toggle button click will be processed
 * SWITCH_ON: map is about to be shown, clicks will be ignored
 * SWITCH_OFF: map is about to be hidden, clicks will be ignored
 */
const STATE = {
  ON: "on",
  OFF: "off",
  SWITCH_ON: "switch_on",
  SWITCH_OFF: "switch_off",
  ERROR: "error",
};

const get_initial_state = (isOn) => {
  if (typeof isOn === "undefined") {
    isOn = get_local_config()["show"];
  }
  if (isOn) {
    return STATE.SWITCH_ON;
  }
  return STATE.SWITCH_OFF;
};

const store_session_state = (state) => {
  const isOn = state === STATE.ON;
  sessionStorage.setItem("caosdb_map.show", JSON.stringify(isOn));
};

const toggle = (container, state, delay) => {
  if (state !== STATE.SWITCH_OFF && state !== STATE.SWITCH_ON) {
    throw new Error(`Illegal state transition from current state '${state}'`);
  }

  const containerRef = document.querySelector(container);
  if (!containerRef) throw new Error(`Map container not found: ${container}.`);

  return new Promise((resolve, reject) => {
    try {
      // TODO animate transition
      var nextState = STATE.OFF;
      if (state === STATE.SWITCH_ON) {
        nextState = STATE.ON;
        containerRef.classList.remove("d-none");
      } else {
        containerRef.classList.add("d-none");
      }

      containerRef.dispatchEvent(
        new Event("caosdb-webui-ext-map.after-toggle")
      );

      setTimeout(() => {
        resolve(nextState);
      }, delay);
    } catch (err) {
      reject(err);
    }
  });
};

const _ToggleMapButton = ({
  tag,
  showInitial,
  className,
  mapContainer,
  labelOn,
  labelOff,
  delay,
  titleOn,
  titleOff,
}) => {
  const [state, setState] = useState(get_initial_state(showInitial));

  const callback = useCallback(() => {
    if (state === STATE.ON) {
      logger.trace("setState", STATE.SWITCH_OFF);
      store_session_state(STATE.OFF);
      setState(STATE.SWITCH_OFF);
    } else if (state === STATE.OFF) {
      logger.trace("setState", STATE.SWITCH_ON);
      store_session_state(STATE.ON);
      setState(STATE.SWITCH_ON);
    }
    // else: no-op
  }, [state, setState]);

  useEffect(() => {
    if (state === STATE.SWITCH_ON || state === STATE.SWITCH_OFF) {
      toggle(mapContainer, state, delay).then(
        (nextState) => {
          logger.trace("setState", nextState);
          setState(nextState);
        },
        (err) => {
          logger.error(err);
          setState(STATE.ERROR);
        }
      );
    }
    // else: no-op
  }, [state, setState, mapContainer, delay]);

  if (state === STATE.ERROR) {
    return <span>{"ERROR"}</span>;
  }

  const label =
    state === STATE.ON || state === STATE.SWITCH_ON ? labelOn : labelOff;

  const props = {
    title: state === STATE.ON || state === STATE.SWITCH_ON ? titleOn : titleOff,
    role: "button",
    href: "#",
    onClick: callback,
  };
  if (className) {
    props["className"] = className;
  }

  const result = React.createElement(tag, props, <span>{label}</span>);
  return result;
};

const ToggleMapButton = (props) => (
  <Await
    promise={get_map_config()}
    then={(config) => config.disabled || <_ToggleMapButton {...props} />}
    loading={<span />}
  />
);

ToggleMapButton.propTypes = {
  tag: PropTypes.oneOf(["a", "button"]),
  className: PropTypes.string,
  mapContainer: PropTypes.string,
  labelOn: PropTypes.string,
  labelOff: PropTypes.string,
  titleOn: PropTypes.string,
  titleOff: PropTypes.string,
  delay: PropTypes.number,
  showInitial: PropTypes.bool,
};

ToggleMapButton.defaultProps = {
  tag: "a",
  className: undefined,
  mapContainer: ".caosdb-f-map-panel",
  labelOn: "Hide Map",
  labelOff: "Show Map",
  titleOn: "Click to hide the map.",
  titleOff: "Click to show the map.",
  delay: 500,
  showInitial: undefined,
};

export { ToggleMapButton };
