import React from "react";
import PropTypes from "prop-types";
import L, { DivIcon } from "leaflet";
import { Marker, Popup } from "react-leaflet";
import { logger } from "./logging";
import {
  Property,
  TransactionService,
} from "@indiscale/caosdb-webui-entity-service";

/**
 * Return an array of components displaying a badge with the parents of an entity.
 *
 * @param {string[]} parents - the parent's names.
 * @returns array of components
 */
const make_parent_labels = function (parents) {
  logger.trace("make_parent_labels", parents);
  return (
    <>
      {parents.map((par, index) => (
        <span className="badge caosdb-f-map-parent-badge" key={index}>
          {par}
        </span>
      ))}
    </>
  );
};

/**
 * Generates a Property Operator Value (POV) expression by chaining the
 * provided arguments with "WITH".
 *
 * @param {string[]} path - array with the names of RecordTypes
 * @returns {string} string with the the filter
 */
const get_with_POV = function (path) {
  var pov = "";
  for (let p of path) {
    pov = pov + ` WITH "${p}" `;
  }
  return pov;
};

/**
 * Generates a Property Operator Value (POV) by joining ids with OR.
 *
 * @param {number[]} ids - array of ids for the filter
 * @returns {string} string with the the filter
 */
const get_id_POV = function (ids) {
  ids = ids.map((x) => "id=" + x);
  return "WITH " + ids.join(" or ");
};

const get_selector = function (path) {
  const sliced = path.slice(1, path.length);
  var selector = sliced.join(".");
  if (selector != "") {
    selector = selector + ".";
  }
  return selector;
};

/**
 * Generates a SELECT query string that applies the provided path of
 * properties as POV and as selector
 *
 * If ids is provided, the condition is not created from the path, but
 * from ids.
 *
 * @param {DataModelConfig} datamodel - datamodel of the entities to be returned.
 * @param {string[]} path - array with the names of RecordTypes
 * @param {number[]} ids - array of ids for the filter
 * @returns {string} query string
 */
const get_select_with_path = function (datamodel, path, ids) {
  if (typeof datamodel === "undefined") {
    throw new Error("Supply the datamodel.");
  }
  if (typeof path === "undefined" || path.length == 0) {
    throw new Error("Supply at least a RecordType.");
  }
  const recordtype = path[0];
  const selector = get_selector(path);
  var pov = undefined;
  if (typeof ids === "undefined") {
    const sliced = path.slice(1, path.length);
    pov =
      get_with_POV(sliced) +
      ` WITH ( "${datamodel.lat}" AND "${datamodel.lng}" )`;
  } else {
    pov = get_id_POV(ids);
  }
  return `SELECT id,name,parent,${selector}${datamodel.lat},${selector}${datamodel.lng} FROM ENTITY "${recordtype}" ${pov} `;
};

/**
 * Component which shows a Popup containing info and links to the entity on the
 * map.
 */
function EntityPopup({ lat, lng, entity, path }) {
  const dms_lat = L.NumberFormatter.toDMS(lat).replace("&deg;", "°");
  const dms_lng = L.NumberFormatter.toDMS(lng).replace("&deg;", "°");

  let extra_loc_hint = "";
  if (path && path.length > 1) {
    extra_loc_hint = (
      <div>{`Location of related ${path[path.length - 1]}`}</div>
    );
  }

  const parent_labels = make_parent_labels(entity.parents);
  const name_label = make_entity_name_label(entity.id, entity.name);
  return (
    <Popup>
      {parent_labels}
      {name_label}
      <div className="small text-muted">
        {extra_loc_hint}
        {`Lat: ${dms_lat} Lng: ${dms_lng}`}
      </div>
    </Popup>
  );
}

EntityPopup.propTypes = {
  path: PropTypes.arrayOf(PropTypes.string),
  entity: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    parents: PropTypes.arrayOf(PropTypes.string),
  }),
  lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

function get_transaction_service() {
  const api = process?.env?.GRPC_API_URI || undefined;
  return new TransactionService(api);
}

// TODO this should be moved to the legacy-adapter
function getBasePath() {
  if (window.connection?.getBasePath) {
    return window.connection.getBasePath();
  }
  return "/";
}

/**
 * Create a div component which shows the name of the entity and contains a
 * link which points to the entity.
 *
 * This is shown as a part of the pop-up when the user click on an
 * entity marker in the map.
 */
const make_entity_name_label = function (id, name) {
  const entity_on_page = !!document.getElementById(id);
  const href = entity_on_page ? `#${id}` : getBasePath() + `Entity/${id}`;
  const title = entity_on_page
    ? "Jump to this entity."
    : "Browse to this entity.";
  return (
    <div className="caosdb-f-map-entity-name-label">
      {name}
      <a href={href} title={title} className="caosdb-f-map-popup-entity-link">
        <i className="bi bi-box-arrow-up-right" />
      </a>
    </div>
  );
};

/**
 * Return the html string for one option in the layer-chooser menu.
 */
function make_layer_chooser_html(icon, name, description) {
  return `<span title="${description}">${icon} ${name}</span>`;
}

/**
 * Single marker component
 */
function EntityMarker({ icon_options, zIndexOffset, lat, lng, path, entity }) {
  if (!icon_options.className) {
    icon_options.className = "";
  }
  const icon = new DivIcon(icon_options);
  return (
    <Marker icon={icon} zIndexOffset={zIndexOffset} position={[lat, lng]}>
      <EntityPopup lat={lat} lng={lng} path={path} entity={entity} />
    </Marker>
  );
}

EntityMarker.propTypes = {
  path: PropTypes.arrayOf(PropTypes.string),
  entity: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    parents: PropTypes.arrayOf(PropTypes.string),
  }),
  lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon_options: PropTypes.object,
  zIndexOffset: PropTypes.number,
};

function isUndefinedOrEmpty (obj) {
  return typeof obj === "undefined" || ( obj.length && obj.length == 0)
}

/**
 * Return a component for markers on the map for an array of entities.
 */
function EntityMarkers({ path, entities, zIndexOffset, icon_options }) {
  var ret = [];
  for (const entity of entities) {
    var lat_vals = entity.lat;
    var lng_vals = entity.lng;

    if (isUndefinedOrEmpty(lng_vals) || isUndefinedOrEmpty(lat_vals)) {
      logger.debug(
        "undefined latitude or longitude",
        entity,
        lat_vals,
        lng_vals
      );
      continue;
    }

    // we need lat_vals and lng_lavs to be arrays so we make them
    // be one
    var is_list_lat = true;
    if (!Array.isArray(lat_vals)) {
      lat_vals = [lat_vals];
      is_list_lat = false;
    }
    var is_list_lng = true;
    if (!Array.isArray(lng_vals)) {
      lng_vals = [lng_vals];
      is_list_lng = false;
    }

    // both array's length must match
    if (
      is_list_lng !== is_list_lat ||
      (is_list_lat && is_list_lng && lat_vals.length !== lng_vals.length)
    ) {
      logger.error(
        "Cannot show this entity on the map. " +
          "Its lat/long properties have different lenghts: ",
        entity
      );
      continue;
    }

    // zip both arrays
    //   [lat1, lat2, ... latN]
    //   [lng1, lng2, ... lngN]
    // into one
    //   [[lat1,lng1],[lat2,lng2],... [latN,lngN]]
    const const_lng_vals_array = lng_vals;
    var latlngs = lat_vals.map(function (e, i) {
      return [e, const_lng_vals_array[i]];
    });

    logger.debug(`create point marker(s) at ${latlngs} for`, entity, zIndexOffset, icon_options);
    for (let latlng of latlngs) {
      ret.push(
        <EntityMarker
          key={ret.length}
          icon_options={icon_options}
          zIndexOffset={zIndexOffset}
          lat={latlng[0]}
          lng={latlng[1]}
          path={path}
          entity={entity}
        />
      );
    }

    /* Code for showing a PATH on the map.
     * Maybe we re-use it later
     *
    logger.debug(`create path line at ${latlngs} for`,
        map_entity);

    var opts = {color:'red', smoothFactor: 10.0, weight: 1.5, opacity: 0.5};
    var opts_2 = {color:'green', smoothFactor: 10.0, weight: 3, opacity: 0.5};
    var path = L.polyline(latlngs, opts);
    if (make_popup) {
        path.bindPopup(make_popup(map_entity, datamodel, lat, lng));
    }
    path.on("mouseover",()=>path.setStyle(opts_2));
    path.on("mouseout",()=>path.setStyle(opts));
    ret.push(path);
     *
     *
     */
  }
  return <>{ret}</>;
}

EntityMarkers.propTypes = {
  path: PropTypes.arrayOf(PropTypes.string),
  zIndexOffset: PropTypes.number,
  icon_options: PropTypes.object,
  entities: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      parents: PropTypes.arrayOf(PropTypes.string),
      lat: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.arrayOf(
          PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        ),
      ]),
      lng: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.arrayOf(
          PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        ),
      ]),
    })
  ),
};

/**
 * Return the value of a single cell in a select result as a POJO.
 *
 * TODO move to caosdb-webui-entity-service
 */
function parseCell(cell) {
  // evil, refactor in caosdb-webui-entity-service
  const property = new Property({ getValue: () => cell });
  return property.getValue();
}

/**
 * Return the select results as a list of POJOs.
 *
 * TODO move to caosdb-webui-entity-service
 */
function get_select_results(select_result) {
  const header = select_result.getHeader();
  if (!header) {
    return [];
  }

  const data_rows = select_result.getDataRowsList();
  if (!data_rows) {
    return [];
  }

  const columns = header.getColumnsList().map((col) => col.getName());
  const data = data_rows.map((row) => {
    const obj = {};

    const cells = row.getCellsList();
    for (let i = 0; i < cells.length; i++) {
      console.assert(
        columns.length === cells.length,
        "broken select result. columns don't match data_rows"
      );

      const key = columns[i];
      const value = parseCell(cells[i]);

      obj[key] = value;
    }

    return obj;
  });

  logger.trace("get_select_results", columns, data);
  return data;
}

export {
  get_select_results,
  get_transaction_service,
  EntityMarkers,
  EntityMarker,
  get_selector,
  make_entity_name_label,
  make_parent_labels,
  get_select_with_path,
  get_id_POV,
  get_with_POV,
  make_layer_chooser_html,
};
