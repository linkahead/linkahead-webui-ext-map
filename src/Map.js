import React, { useCallback, useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  LayersControl,
  MapContainer,
  TileLayer,
  WMSTileLayer,
  useMap,
} from "react-leaflet";
import L from "leaflet";
import LProj from "proj4leaflet";
import { CurrentPageEntities } from "./CurrentPageEntities";
import { AllMapEntities } from "./AllMapEntities";
import { get_map_config } from "./MapConfig";
import { ViewChangeControl } from "./components/ViewChangeControl";
import { SearchControl } from "./components/SearchControl";
import { Coordinates } from "./components/Coordinates";
import { Graticule } from "./components/Graticule";
import { PathDropDown } from "./components/PathDropDown";
import { Await } from "@indiscale/caosdb-webui-core-components";
import { logger } from "./logging";
import default_config from "./default_config.json";

L.Proj = L.Proj || LProj;

/**
 * Create a Leaflet tile layer.
 *
 * @param {TileLayerConfig} config - configuration for the map.
 *
 * @returns {L.TileLayer} the tile layer
 */
const create_tile_layer = function (config) {
  logger.trace("enter create_tile_layer", config);

  // set tiling server
  config.options = config.options || {};
  if (config.type && config.type === "wms") {
    return <WMSTileLayer url={config.url} {...config.options} />;
  } else if (config.type === "osm" || typeof config.type === "undefined") {
    return <TileLayer url={config.url} {...config.options} />;
  } else {
    throw new Error("unknown tileLayer type: " + config.type);
  }
};

function get_crs_config(config) {
  var crs = L.CRS.EPSG3857;
  if (typeof config.crs === "string" || config.crs instanceof String) {
    crs = L.CRS[config.crs.replace(":", "")];
    logger.debug("use pre-defined CRS ", crs);
  } else if (typeof config.crs === "object") {
    crs = new L.Proj.CRS(
      config.crs.code,
      config.crs.proj4def,
      config.crs.options
    );
    logger.debug("use custom proj4 CRS ", crs);
  }
  return crs;
}

function ResizeListener() {
  const map = useMap();

  useEffect(() => {
    map
      .getContainer()
      .parentElement.addEventListener(
        "caosdb-webui-ext-map.after-toggle",
        (e) => {
          logger.trace("ResizeListener", e);
          map.invalidateSize(true);
        }
      );
  }, [map]);

  return null;
}

function get_last_path_id() {
  var old = sessionStorage.getItem("caosdb_map.display_path");
  if (old === "undefined") {
    // more robust
    return null;
  }
  return old;
}

function EntityLayers({ queryCallback, config }) {
  const [pathId, setPathId] = useState(get_last_path_id() || "same");

  const currentPath = config.select.paths[pathId] || [];
  logger.trace("EntityLayers", queryCallback, config, pathId, currentPath);

  useEffect(() => {
    if (pathId !== "same") {
      sessionStorage.setItem("caosdb_map.display_path", pathId);
    } else {
      sessionStorage.removeItem("caosdb_map.display_path");
    }
  }, [pathId]);

  const datamodel = config.datamodel;
  if (!config.select.query) {
    config.select.query = default_config.select.query;
  }
  datamodel.role = config.select.query.role;
  datamodel.entity = config.select.query.entity;

  return (
    <>
      <SearchControl
        queryCallback={queryCallback}
        currentPath={currentPath}
        datamodel={config.datamodel}
      />

      <LayersControl>
        {!config.entityLayers.current_page_entities || (
          <CurrentPageEntities
            currentPath={currentPath}
            datamodel={config.datamodel}
            {...config.entityLayers.current_page_entities}
          />
        )}
        {!config.entityLayers.all_map_entities || (
          <AllMapEntities
            currentPath={currentPath}
            datamodel={config.datamodel}
            {...config.entityLayers.all_map_entities}
          />
        )}
      </LayersControl>
      <PathDropDown
        currentPath={pathId}
        setCurrentPath={setPathId}
        paths={config.select.paths}
      />
    </>
  );
}

EntityLayers.propTypes = {
  queryCallback: PropTypes.func,
  config: PropTypes.shape({
    datamodel: PropTypes.object.isRequired,
    select: PropTypes.object.isRequired,
    entityLayers: PropTypes.object.isRequired,
  }),
};

function ConfigureMap({ config, initialView, queryCallback }) {
  logger.trace(config, initialView, queryCallback);
  const [mapView, setMapView] = useState(initialView || config.default_view);

  const _setMapView = useCallback(
    (view) => {
      logger.trace("_setMapView", view);
      if (view) {
        sessionStorage.setItem("caosdb_map.view", JSON.stringify(view));
      } else {
        sessionStorage.removeItem("caosdb_map.view");
      }
      setMapView(view || config.default_view);
    },
    [setMapView, config.default_view]
  );

  if (config.disabled) {
    return <div />;
  }

  var view_config = undefined;
  config.views.forEach((view) => {
    if (view.id === mapView) {
      view_config = view;
    }
  });

  const tileLayer = create_tile_layer(view_config.tileLayer);

  const map_options = {};
  map_options.boxZoom = view_config.boxZoom || false;
  map_options.center = view_config.center || [0, 0];
  map_options.zoom = view_config.zoom || 10;

  const crs_config = get_crs_config(view_config);
  if (crs_config) {
    map_options.crs = crs_config;
  }

  return (
    <MapContainer
      key={mapView}
      className="map"
      scrollWheelZoom={true}
      {...map_options}
    >
      {tileLayer}
      <ResizeListener />
      <Coordinates />
      {view_config.graticule && <Graticule config={view_config.graticule} />}
      <ViewChangeControl
        mapView={mapView}
        setMapView={_setMapView}
        views={config.views}
      />
      <EntityLayers queryCallback={queryCallback} config={config} />
    </MapContainer>
  );
}

ConfigureMap.propTypes = {
  queryCallback: PropTypes.func,
  initialView: PropTypes.string,
  config: PropTypes.object,
};

ConfigureMap.defaultProps = {
  queryCallback: (query) => {
    logger.error("Unconfigured queryCallback", query);
  },
  config: undefined,
};

export function Map({ queryCallback, initialView }) {
  return (
    <Await
      promise={get_map_config()}
      then={(map_config) => (
        <ConfigureMap
          config={map_config}
          initialView={initialView}
          queryCallback={queryCallback}
        />
      )}
    />
  );
}

Map.propTypes = {
  queryCallback: PropTypes.func,
  initialView: PropTypes.string,
};
