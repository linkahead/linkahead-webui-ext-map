import React from "react";
import PropTypes from "prop-types";
import { logger } from "./logging";
import { LayerGroup, LayersControl } from "react-leaflet";
import {
  get_select_results,
  get_selector,
  get_transaction_service,
  get_select_with_path,
  EntityMarkers,
  make_layer_chooser_html,
} from "./Map.helpers";
import { Await } from "@indiscale/caosdb-webui-core-components";

// TODO the following functions are adapters to the legacy web interface.
// Currently there doesn't seem to be a way to get rid of them easily, but the
// should move to caosdb-webui-legacy-adapter some time.
function _getEntityName(entity) {
  if (window.getEntityName) {
    return window.getEntityName(entity);
  }
  throw new Error("no implementation for getEntityName");
}

function _getProperty(entity, property) {
  if (window.getProperty) {
    return window.getProperty(entity, property);
  }
  throw new Error("no implementation for getProperty");
}

function _getEntityID(entity) {
  if (window.getEntityID) {
    return window.getEntityID(entity);
  }
  throw new Error("no implementation for getEntityID");
}

function _getParents(entity) {
  if (window.getParents) {
    return window.getParents(entity);
  }
  throw new Error("no implementation for getParents");
}

function _getEntities() {
  if (window.getEntities) {
    return window.getEntities();
  }
  throw new Error("no implementation for getEntities");
}

/**
 * Async wrapper for EntityMarkers.
 */
const _create_entity_markers = async function (
  currentPath,
  entities,
  datamodel,
  zIndexOffset,
  icon_options
) {
  const resolved_entities = await entities;
  return (
    <EntityMarkers
      path={currentPath}
      entities={resolved_entities}
      zIndexOffset={zIndexOffset}
      icon_options={icon_options}
    />
  );
};

/**
 * Return the HTMLElements representing entities displayable on the map.
 */
function get_map_entities(container, datamodel) {
  if (!container) {
    return [];
  }
  var map_entities = [
    ...container.getElementsByClassName("caosdb-entity-panel"),
  ].filter((entity) => {
    var ret = 0;
    for (let e of entity.getElementsByClassName("caosdb-property-name")) {
      if (e.textContent.trim() === datamodel.lat) {
        ret += 1;
      } else if (e.textContent.trim() === datamodel.lng) {
        ret += 1;
      }
    }
    return ret === 2;
  });

  return map_entities;
}

/**
 * Return an array of objects representing the entities on the current page,
 * given the datamodel and the currently selected path.
 */
async function get_current_page_entities(datamodel, path) {
  logger.trace("get_current_page_entities", datamodel, path);

  if (typeof path !== "undefined" && path.length) {
    var ids = [];
    for (let rec of _getEntities()) {
      ids.push(_getEntityID(rec));
    }
    if (ids.length) {
      const service = get_transaction_service();
      const query = get_select_with_path(datamodel, path, ids);
      logger.trace(`get_current_page_entities query: ${query}`);
      const response = await service.executeQuery(query);
      try {
        const entities = get_select_results(
          response.getResponsesList()[0].getRetrieveResponse().getSelectResult()
        ).map((obj) => {
          const selector = get_selector(path);
          return {
            parents: obj["parent"],
            id: obj.id,
            name: obj.name,
            lat: obj[`${selector}${datamodel.lat}`],
            lng: obj[`${selector}${datamodel.lng}`],
          };
        });

        logger.trace("get_current_page_entities: ", entities);
        return entities;
      } catch (err) {
        logger.error(err, response);
      }
    }

    return [];
  }

  const container = document.getElementsByClassName(
    "caosdb-f-main-entities"
  )[0];
  return get_map_entities(container, datamodel).map((map_entity) => {
    return {
      name: _getEntityName(map_entity),
      id: _getEntityID(map_entity),
      parents: _getParents(map_entity).map((par) => par.name),
      lat: _getProperty(map_entity, datamodel.lat),
      lng: _getProperty(map_entity, datamodel.lng),
    };
  });
}

export function CurrentPageEntities({
  currentPath,
  datamodel,
  icon_options,
  zIndexOffset,
  name,
  description,
  active,
}) {
  var entities = get_current_page_entities(datamodel, currentPath);
  var markers = _create_entity_markers(
    currentPath,
    entities,
    datamodel,
    zIndexOffset,
    icon_options
  );

  var control_options = {
    name: make_layer_chooser_html(icon_options.html, name, description),
  };
  if (active) {
    control_options.checked = true;
  }

  return (
    <LayersControl.Overlay {...control_options}>
      <Await
        loading={null}
        promise={markers}
        then={(markers) => <LayerGroup>{markers}</LayerGroup>}
      />
      ;
    </LayersControl.Overlay>
  );
}

CurrentPageEntities.propTypes = {
  currentPath: PropTypes.arrayOf(PropTypes.string),
  datamodel: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
    role: PropTypes.string,
    entity: PropTypes.string,
  }),
  zIndexOffset: PropTypes.number,
  icon_options: PropTypes.object,
  name: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
};
