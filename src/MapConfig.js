import { logger } from "./logging";
import default_config from "./default_config.json";

const fetch_config = async function () {
  try {
    const response = await window.fetch("/webinterface/conf/json/ext_map.json");
    if (response.status === 404) {
      logger.warn("couldn't find the map config file");
      return {};
    }
    const local_config = await response.json();
    return local_config;
  } catch (err) {
    logger.error(err);
    return {};
  }
};

const load_map_config = async function () {
  const remote_config = await fetch_config();
  const local_config = get_local_config();

  // TODO implement merge, not concat (because deep properties will be lost).
  const map_config = { ...default_config, ...remote_config, ...local_config };
  return map_config;
};

const get_from_session_storage = function (key) {
  try {
    var val = sessionStorage.getItem(key);
    if (val && val !== "undefined") {
      return JSON.parse(val);
    }
  } catch (err) {
    logger.error(err);
  }
  return undefined;
};

export function get_local_config() {
  const result = {};
  var val = get_from_session_storage("caosdb_map.view");
  if (val) {
    result["view"] = val;
  }

  val = get_from_session_storage("caosdb_map.show");
  if (val) {
    result["show"] = val;
  }

  return result;
}

var map_config = undefined;

export async function get_map_config() {
  if (!map_config) {
    map_config = await load_map_config();
  }
  return map_config;
}
