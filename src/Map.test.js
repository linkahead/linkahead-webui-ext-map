import React from "react";
import default_config from "./default_config.json";
import { get_map_config } from "./MapConfig";
import { act, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Map } from "./Map";

jest.mock("./MapConfig");
jest.mock("./CurrentPageEntities", () => {
  return {
    __esModule: true,
    CurrentPageEntities: () => {
      return <div data-testid="currect-page-entities" />;
    },
  };
});
jest.mock("./AllMapEntities", () => {
  return {
    __esModule: true,
    AllMapEntities: () => {
      return <div data-testid="all-map-entities" />;
    },
  };
});

const sleep = function (ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

describe("Map basic properties", () => {
  test("shows loading until config is there", async () => {
    get_map_config.mockReturnValue(
      new Promise((resolve) => {
        setTimeout(() => resolve(default_config), 1);
      })
    );

    const { container } = render(<Map />);
    expect(container).toMatchSnapshot();

    var spinner = screen.getByTitle(/Loading/i);
    expect(spinner).toBeInTheDocument();
    expect(container.firstChild.className).toBe("spinner-border");

    await act(async () => {
      await sleep(1000);
    });

    spinner = screen.queryByTitle(/Loading/i);
    expect(spinner).toBeNull();

    expect(container.firstChild.className).toContain("map");
    expect(container.firstChild.className).toContain("leaflet-container");
  });
});
