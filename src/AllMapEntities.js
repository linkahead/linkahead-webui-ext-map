import React from "react";
import PropTypes from "prop-types";
import { logger } from "./logging";
import { Await } from "@indiscale/caosdb-webui-core-components";
import { LayerGroup, LayersControl } from "react-leaflet";
import {
  get_select_results,
  get_transaction_service,
  EntityMarkers,
  get_selector,
  make_layer_chooser_html,
  get_select_with_path,
} from "./Map.helpers";

/**
 * Generate and return a query for the map entities.
 */
function _make_all_map_entities_query(datamodel, path) {
  if (typeof path !== "undefined" && path.length) {
    return get_select_with_path(datamodel, path);
  }
  var ret = `SELECT id,name,parent,${datamodel.lat},${datamodel.lng} FROM ${datamodel.role}`;
  if (datamodel.entity) {
    ret += ` "${datamodel.entity}"`;
  }
  ret += ` WITH "${datamodel.lat}" AND "${datamodel.lng}"`;
  return ret;
}

/**
 * Retrieve all displayable entities, given the datamodel and the currently selected path.
 */
async function get_all_map_entities(datamodel, path) {
  const service = get_transaction_service();
  const query = _make_all_map_entities_query(datamodel, path);
  logger.debug(`get_all_map_entities query: ${query}`, datamodel, path);
  const response = await service.executeQuery(query);
  try {
    const entities = get_select_results(
      response.getResponsesList()[0].getRetrieveResponse().getSelectResult()
    ).map((obj) => {
      const selector = get_selector(path);
      return {
        parents: obj["parent"],
        id: obj.id,
        name: obj.name,
        lat: obj[`${selector}${datamodel.lat}`],
        lng: obj[`${selector}${datamodel.lng}`],
      };
    });

    logger.debug("get_all_map_entities entities: ", entities);
    return entities;
  } catch (err) {
    logger.error(err, response);
  }

  return [];
}

/**
 * Async wrapper around EntityMarkers component.
 */
async function _create_entity_markers(
  path,
  entities,
  zIndexOffset,
  icon_options
) {
  var resolved_entities = await entities;
  return (
    <EntityMarkers
      path={path}
      entities={resolved_entities}
      zIndexOffset={zIndexOffset}
      icon_options={icon_options}
    />
  );
}

/**
 * Component which displays all displayable entities in the database on the map.
 */
export function AllMapEntities({
  currentPath,
  zIndexOffset,
  icon_options,
  datamodel,
  name,
  description,
  active,
}) {
  logger.trace(
    "AllMapEntities",
    currentPath,
    zIndexOffset,
    icon_options,
    datamodel
  );
  zIndexOffset = zIndexOffset || 0;

  var entities = get_all_map_entities(datamodel, currentPath);
  var markers = _create_entity_markers(
    currentPath,
    entities,
    zIndexOffset,
    icon_options
  );

  var control_options = {
    name: make_layer_chooser_html(icon_options.html, name, description),
  };
  if (active) {
    control_options.checked = true;
  }

  return (
    <LayersControl.Overlay {...control_options}>
      <Await
        loading={null}
        promise={markers}
        then={(markers) => <LayerGroup>{markers}</LayerGroup>}
      />
      ;
    </LayersControl.Overlay>
  );
}

AllMapEntities.propTypes = {
  currentPath: PropTypes.arrayOf(PropTypes.string),
  datamodel: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
    role: PropTypes.string,
    entity: PropTypes.string,
  }),
  zIndexOffset: PropTypes.number,
  icon_options: PropTypes.object,
  name: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
};
