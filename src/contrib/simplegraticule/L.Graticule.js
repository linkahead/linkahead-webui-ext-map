// MIT License
//
// Copyright © 2013 turban, tmcw
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

import * as L from "leaflet";

/*
 Graticule plugin for Leaflet powered maps.
*/
// eslint-disable-next-line no-import-assign
L.Graticule = L.GeoJSON.extend({
  options: {
    style: {
      color: "#333",
      weight: 1,
    },
    interval: 20,
  },

  initialize: function (options) {
    L.Util.setOptions(this, options);
    this._layers = {};

    if (this.options.sphere) {
      this.addData(this._getFrame());
    } else {
      this.addData(this._getGraticule());
    }
  },

  _getFrame: function () {
    return {
      type: "Polygon",
      coordinates: [
        this._getMeridian(-180).concat(this._getMeridian(180).reverse()),
      ],
    };
  },

  _getGraticule: function () {
    var features = [],
      interval = this.options.interval;

    // Meridians
    for (var lng = 0; lng <= 180; lng = lng + interval) {
      features.push(
        this._getFeature(this._getMeridian(lng), {
          name: lng ? lng.toString() + "° E" : "Prime meridian",
        })
      );
      if (lng !== 0) {
        features.push(
          this._getFeature(this._getMeridian(-lng), {
            name: lng.toString() + "° W",
          })
        );
      }
    }

    // Parallels
    for (var lat = 0; lat <= 90; lat = lat + interval) {
      features.push(
        this._getFeature(this._getParallel(lat), {
          name: lat ? lat.toString() + "° N" : "Equator",
        })
      );
      if (lat !== 0) {
        features.push(
          this._getFeature(this._getParallel(-lat), {
            name: lat.toString() + "° S",
          })
        );
      }
    }

    return {
      type: "FeatureCollection",
      features: features,
    };
  },

  _getMeridian: function (lng) {
    lng = this._lngFix(lng);
    var coords = [];
    for (var lat = -90; lat <= 90; lat++) {
      coords.push([lng, lat]);
    }
    return coords;
  },

  _getParallel: function (lat) {
    var coords = [];
    for (var lng = -180; lng <= 180; lng++) {
      coords.push([this._lngFix(lng), lat]);
    }
    return coords;
  },

  _getFeature: function (coords, prop) {
    return {
      type: "Feature",
      geometry: {
        type: "LineString",
        coordinates: coords,
      },
      properties: prop,
    };
  },

  _lngFix: function (lng) {
    if (lng >= 180) return 179.999999;
    if (lng <= -180) return -179.999999;
    return lng;
  },
});

// eslint-disable-next-line no-import-assign
L.graticule = function (options) {
  return new L.Graticule(options);
};
