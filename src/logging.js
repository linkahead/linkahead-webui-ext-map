import log from "loglevel";

const logger = log.getLogger("caosdb-webui-ext-map");

export { logger };
