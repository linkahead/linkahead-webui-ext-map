# CaosDB Webui Ext Map

Map extension for the CaosDB Webui based on Leaflet and React.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run format`

Runs the formatter (prettier) on the `src/` directory.

### `npm run lint`

Runs the linter (eslint) on the `src/` directory.

## Deployment (with LinkAhead)

1. Build the bundle in the react map repo with `npm run build`.
2. Copy the build directory to `custom/caosdb-server/caosdb-webui/src/ext/include/reactmap`.
3. Provide a `ext_map.json` under `custom/caosdb-server/caosdb-webui/conf/ext/json/ext_map.json`.
4. Start linkahead with envoy enabled and with webui ref = `f-react-map`.
5. Browse to localhost:8081. Optionally insert test data via `./misc/map_test_data.py`.
