import React from "react";
import ReactDOM from "react-dom/client";
import "../src/index.scss";
import "bootstrap/scss/bootstrap.scss";
import "bootstrap-icons/font/bootstrap-icons.css";
import "regenerator-runtime/runtime";
import { Map } from "../src/Map";
import { ToggleMapButton } from "../src/components/ToggleMapButton";

window.getEntities = () => []

const caosdb_webui_ext_map = {
  init: (container, toggle_button) => {
    const root = ReactDOM.createRoot(container);
    root.render(
      <React.StrictMode>
        <Map />
      </React.StrictMode>
    );

    const root2 = ReactDOM.createRoot(toggle_button);
    root2.render(
      <React.StrictMode>
        <ToggleMapButton mapContainer="#caosdb-f-map-panel"/>
      </React.StrictMode>
    );

  },
};

caosdb_webui_ext_map.init(document.getElementById("caosdb-f-map-panel"), document.getElementById("caosdb-f-toggle-map-button"));
